(function ($) {
	"use strict";
	
	$(document).ready(function($){
		
		//carousel active
		$(".carousel-inner .item:first-child").addClass("active");
		
		//Fixed nav on scroll
		$(document).scroll(function(e){
			var scrollTop = $(document).scrollTop();
			if(scrollTop > $('nav').height()){
				//console.log(scrollTop);
				$('nav').addClass('navbar-fixed-top');
			}
			else {
				$('nav').removeClass('navbar-fixed-top');
			}
		});
			
		//Portfolio Popup
		$('.magnific-popup').magnificPopup({type:'image'});
		
	});


	$(window).on('load',function(){
		
		//Preloader
		$('.preloader').delay(500).fadeOut('slow');
        $('body').delay(500).css({'overflow':'visible'});
		
		//Numaric Counter
		$('.counter').counterUp({
			delay: 10,
			time: 1000
		});		
				
		//Portfolio container			
		var $container = $('.portfolioContainer');
		$container.isotope({
			filter: '*',
			animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false
			}
		});
		
		$('.portfolioFilter a').on('click', function(){
			$('.portfolioFilter a').removeClass('current');
			$(this).addClass('current');
			
			var selector = $(this).attr('data-filter');
			$container.isotope({
				filter: selector,
				animationOptions: {
					duration: 750,
					easing: 'linear',
					queue: false
				}
			 });
			 return false;
		}); 
	
	});
	
	//Wow js
	new WOW().init();
	 
}(jQuery));	