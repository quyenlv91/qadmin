<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Admin group
Route::group(['prefix' => env('APP_ADMIN_ROUTE'), 'middleware' => ['admin.check']], function() {
	Route::get('', 'Admin\HomeController@index');
	Auth::routes();
	Route::get('home', 'Admin\HomeController@index');
	Route::put('users/group_action', ['as'=> 'admin.users.group_action', 'uses' => 'Admin\UsersController@group_action']);
	Route::resource('users', 'Admin\UsersController');
	Route::resource('settings/roles', 'Admin\RolesController');
	Route::resource('settings/permissions', 'Admin\PermissionsController');
	// ajax upload images
	Route::match(array('POST', 'PATCH'),'upload/images', 'Admin\UploadImageController@index');
	Route::delete('delete/images/{image_name}', 'Admin\UploadImageController@delete');
	Route::get('get_images/{table}/{field}/{id}', 'Admin\UploadImageController@get_images');

	Route::post('navigations/save_orders', 'Admin\NavigationsController@saveOrders');
	Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder');
	Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate');
	Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate');
	
});
// admin middleware except Login page and Login method
Route::get('/'.env('APP_ADMIN_ROUTE').'/login', 'Auth\LoginController@showLoginForm');
Route::post('/'.env('APP_ADMIN_ROUTE').'/login', 'Auth\LoginController@login');
Route::get('/'.env('APP_ADMIN_ROUTE').'/register', 'Auth\RegisterController@showRegistrationForm');
Route::post('/'.env('APP_ADMIN_ROUTE').'/register', 'Auth\RegisterController@register');
Route::post('/'.env('APP_ADMIN_ROUTE').'/password/reset', 'Auth\RegisterController@register');
Route::get('/'.env('APP_ADMIN_ROUTE').'/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('admin/slides', ['as'=> 'admin.slides.index', 'uses' => 'Admin\SlidesController@index']);
Route::post('admin/slides', ['as'=> 'admin.slides.store', 'uses' => 'Admin\SlidesController@store']);
Route::get('admin/slides/create', ['as'=> 'admin.slides.create', 'uses' => 'Admin\SlidesController@create']);
Route::put('admin/slides/group_action', ['as'=> 'admin.slides.group_action', 'uses' => 'Admin\SlidesController@group_action']);
Route::put('admin/slides/{slides}', ['as'=> 'admin.slides.update', 'uses' => 'Admin\SlidesController@update']);
Route::patch('admin/slides/{slides}', ['as'=> 'admin.slides.update', 'uses' => 'Admin\SlidesController@update']);
Route::delete('admin/slides/{slides}', ['as'=> 'admin.slides.destroy', 'uses' => 'Admin\SlidesController@destroy']);
Route::get('admin/slides/{slides}', ['as'=> 'admin.slides.show', 'uses' => 'Admin\SlidesController@show']);
Route::get('admin/slides/{slides}/edit', ['as'=> 'admin.slides.edit', 'uses' => 'Admin\SlidesController@edit']);

Route::get('admin/settings/commons', ['as'=> 'admin.commons.index', 'uses' => 'Admin\CommonController@index']);
Route::post('admin/settings/commons', ['as'=> 'admin.commons.store', 'uses' => 'Admin\CommonController@store']);
Route::get('admin/settings/commons/create', ['as'=> 'admin.commons.create', 'uses' => 'Admin\CommonController@create']);
Route::put('admin/settings/commons/group_action', ['as'=> 'admin.commons.group_action', 'uses' => 'Admin\CommonController@group_action']);
Route::put('admin/settings/commons/{commons}', ['as'=> 'admin.commons.update', 'uses' => 'Admin\CommonController@update']);
Route::patch('admin/settings/commons/{commons}', ['as'=> 'admin.commons.update', 'uses' => 'Admin\CommonController@update']);
Route::delete('admin/settings/commons/{commons}', ['as'=> 'admin.commons.destroy', 'uses' => 'Admin\CommonController@destroy']);
Route::get('admin/settings/commons/{commons}', ['as'=> 'admin.commons.show', 'uses' => 'Admin\CommonController@show']);
Route::get('admin/settings/commons/{commons}/edit', ['as'=> 'admin.commons.edit', 'uses' => 'Admin\CommonController@edit']);


Route::get('admin/posts', ['as'=> 'admin.posts.index', 'uses' => 'Admin\PostController@index']);
Route::post('admin/posts', ['as'=> 'admin.posts.store', 'uses' => 'Admin\PostController@store']);
Route::get('admin/posts/create', ['as'=> 'admin.posts.create', 'uses' => 'Admin\PostController@create']);
Route::put('admin/posts/group_action', ['as'=> 'admin.posts.group_action', 'uses' => 'Admin\PostController@group_action']);
Route::put('admin/posts/{posts}', ['as'=> 'admin.posts.update', 'uses' => 'Admin\PostController@update']);
Route::patch('admin/posts/{posts}', ['as'=> 'admin.posts.update', 'uses' => 'Admin\PostController@update']);
Route::delete('admin/posts/{posts}', ['as'=> 'admin.posts.destroy', 'uses' => 'Admin\PostController@destroy']);
Route::get('admin/posts/{posts}', ['as'=> 'admin.posts.show', 'uses' => 'Admin\PostController@show']);
Route::get('admin/posts/{posts}/edit', ['as'=> 'admin.posts.edit', 'uses' => 'Admin\PostController@edit']);
