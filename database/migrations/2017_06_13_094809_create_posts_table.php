<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Models\Admin\Navigations;

class CreatePostsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('content');
            $table->boolean('active')->default(0);
            $table->timestamps();
        });
        // Insert record to permissions table
        $id = DB::table('permissions')->insertGetId([
                'name' => 'Manage Post',
                'created_at' =>  date("Y-m-d H:i:s")
            ]);
        DB::table('role_has_permissions')->insert([
                'role_id' => 1,
                'permission_id' => $id
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
        DB::table('permissions')->where('name', '=', 'Manage Post')->delete();
    }
}
