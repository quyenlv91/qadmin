<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = DB::table('users')->where('name', '=', 'Super Admin')->count();
        if($check == 0)
        {
            DB::table('users')->insert([
                'name' => 'Super Admin',
                'email' => 'admin@google.com',
                'avatar' => 'admin.png',
                'password' => bcrypt('123456'),
                'role' => 1,
                'active' => 1,
            ]);

            DB::table('roles')->insertGetId([
                'id' => 1,
                'name' => 'Super Admin',
            ]);

            DB::table('user_has_roles')->insert([
                'role_id' => 1,
                'user_id' => 1,
            ]);

            DB::table('permissions')->insert([
                ['name' => 'Manage Users'],
                ['name' => 'Manage Roles'],
                ['name' => 'Manage Permissions'],
                ['name' => 'Manage Navigations'],
                ['name' => 'Manage Slides']
            ]);

            DB::table('role_has_permissions')->insert([
                ['role_id' => 1, 'permission_id' => 1],
                ['role_id' => 1, 'permission_id' => 2],
                ['role_id' => 1, 'permission_id' => 3],
                ['role_id' => 1, 'permission_id' => 4],
                ['role_id' => 1, 'permission_id' => 5]
            ]);

            DB::table('content_types')->insert([
                ['id' => 1, 'name' => 'new', 'is_default' => 1],
                ['id' => 2, 'name' => 'product', 'is_default' => 0],
            ]);
        }
    }
}
