@servers(['develop' => '-i /var/lib/jenkins/id_rsa quyenlv@192.168.0.111', 'test' => '-i /home/quyenlv/id_rsa quangdv@192.168.0.247', 'production' => '-i /home/quyenlv/id_rsa quangdv@192.168.0.247'])

@setup
    $listEnv = ['develop', 'test', 'production'];
    $gitSSH = 'git@git.vsii.com:quangdv/laravel-sot.git';
    $developEnv = [
                    'http://lavarel.local', // url
                    'laravel_develop', // db Name
                    'root', // db user name
                    '123456', // db password
                    'localhost', // host
                    '/home/quyenlv/www/',  // folder contain code
                ];
    $testEnv = [
                    'http://lavarel.local', // url
                    'laravel_soft', // db Name
                    'root', // db user name
                    '261088', // db password
                    'localhost', // host
                    '/home/quangdv/www/',  // folder contain code
                ];
    $productionEnv = [
                    'http://lavarel.local', // url
                    'laravel_soft', // db Name
                    'root', // db user name
                    '261088', // db password
                    'localhost', // host
                    '/home/quangdv/www/',  // folder contain code
                ];
    $nameEnv = (isset($env) && in_array($env, $listEnv)) ? $nameEnv = $env . 'Env' : 'testEnv';
    list($url, $dbName, $dbUser, $dbPwd, $host, $wwwRoot) = $$nameEnv;
@endsetup

@story('deploy')
    @if(isset($env) && in_array($env, $listEnv))
        deploy_{{ $env }}
    @else
        {{ $env }}
    @endif
@endstory

@task('deploy_develop', ['on' => 'develop'])
    cd {{ $wwwRoot }}
    [ ! -d {{ $wwwRoot }}laravel-sot ] && git clone {{ $gitSSH }}
    cd {{ $wwwRoot }}laravel-sot/
    git fetch && git pull origin master
    git checkout develop
    sudo chmod -R 777 {{ $wwwRoot }}laravel-sot/storage/
    composer install
    rm -rf .env
    cp .env.example .env
    sed -i 's,APP_URL=http://localhost,APP_URL={{ $url }},g' .env
    sed -i 's/DB_DATABASE=homestead/DB_DATABASE={{ $dbName }}/g' .env
    sed -i 's/DB_USERNAME=homestead/DB_USERNAME={{ $dbUser }}/g' .env
    sed -i 's/DB_PASSWORD=secret/DB_PASSWORD={{ $dbPwd }}/g' .env
    php artisan key:generate
    mysql -u{{ $dbUser }} -p{{ $dbPwd }} -e "CREATE DATABASE IF NOT EXISTS {{ $dbName }} CHARACTER SET utf8 COLLATE utf8_general_ci;GRANT ALL PRIVILEGES ON {{ $dbName }}.* TO '{{ $dbUser }}'@'{{ isset($host) ? $host : 'localhost' }}';"
    php artisan migrate
    php artisan db:seed
    echo 'done...';
@endtask

@task('deploy_test', ['on' => 'test'])
    cd {{ $wwwRoot }}
    [ ! -d {{ $wwwRoot }}laravel-sot ] && git clone {{ $gitSSH }}
    cd {{ $wwwRoot }}laravel-sot
    git fetch && git pull origin master
    composer install
    rm -rf .env
    cp .env.example .env
    sed -i 's,APP_URL=http://localhost,APP_URL={{ $url }},g' .env
    sed -i 's/DB_DATABASE=homestead/DB_DATABASE={{ $dbName }}/g' .env
    sed -i 's/DB_USERNAME=homestead/DB_USERNAME={{ $dbUser }}/g' .env
    sed -i 's/DB_PASSWORD=secret/DB_PASSWORD={{ $dbPwd }}/g' .env
    php artisan key:generate
    mysql -u{{ $dbUser }} -p{{ $dbPwd }} -e "CREATE DATABASE IF NOT EXISTS {{ $dbName }} CHARACTER SET utf8 COLLATE utf8_general_ci;GRANT ALL PRIVILEGES ON {{ $dbName }}.* TO '{{ $dbUser }}'@'{{ isset($host) ? $host : 'localhost' }}';"
    php artisan migrate
    php artisan db:seed
    echo "Deploy completed!";
@endtask

@task('deploy_production', ['on' => 'production'])
    cd {{ $wwwRoot }}
    [ ! -d {{ $wwwRoot }}laravel-sot ] && git clone {{ $gitSSH }}
    cd {{ $wwwRoot }}laravel-sot
    git fetch && git pull origin master
    composer install
    rm -rf .env
    cp .env.example .env
    sed -i 's,APP_URL=http://localhost,APP_URL={{ $url }},g' .env
    sed -i 's/DB_DATABASE=homestead/DB_DATABASE={{ $dbName }}/g' .env
    sed -i 's/DB_USERNAME=homestead/DB_USERNAME={{ $dbUser }}/g' .env
    sed -i 's/DB_PASSWORD=secret/DB_PASSWORD={{ $dbPwd }}/g' .env
    php artisan key:generate
    mysql -u{{ $dbUser }} -p{{ $dbPwd }} -e "CREATE DATABASE IF NOT EXISTS {{ $dbName }} CHARACTER SET utf8 COLLATE utf8_general_ci;GRANT ALL PRIVILEGES ON {{ $dbName }}.* TO '{{ $dbUser }}'@'{{ isset($host) ? $host : 'localhost' }}';"
    php artisan migrate
    php artisan db:seed
    echo "Deploy completed!";
@endtask