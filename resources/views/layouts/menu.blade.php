<li>
    <a class="waves-effect {{ Request::is('*admin/home*') ? 'active' : '' }}" href="{{ url('admin/home') }}"> <i class="fa fa-home"></i><span> Home </span> </a>
</li>
<li class="has_sub">
    <a class="waves-effect {{ Request::is('*admin/settings*') ? 'active' : '' }}" href="javascript:void(0)"> <i class="fa fa-wrench"></i><span> Settings </span></a>
    <ul id="settings-sub-menu">
    	<li class="{{ Request::is('*admin/settings/commons*') ? 'active' : '' }}">
    		<a href="{{ url('admin/settings/commons') }}">Common</a></li>
    	<li class="{{ Request::is('*admin/settings/roles*') ? 'active' : '' }}">
		    <a href="{{ url('admin/settings/roles') }}">Manage Roles</a>
		</li>
		<li class="{{ Request::is('*admin/settings/permissions*') ? 'active' : '' }}">
		    <a href="{{ url('admin/settings/permissions') }}">Manage Permissions</a>
		</li>
    </ul>
</li>
<li>
    <a class="waves-effect {{ Request::is('*admin/users*') ? 'active' : '' }}" href="{{ url('admin/users') }}"> <i class="fa fa-users"></i><span> Manage Users </span> </a>
</li>
<li>
    <a class="waves-effect {{ Request::is('*admin/slides*') ? 'active' : '' }}" href="{{ url('admin/slides') }}"> <i class="fa fa-sliders"></i><span> Manage Slides </span> </a>
</li>
