@extends('layouts.app')

@section('titleWS')
	{{ env('APP_NAME_UP') }} - Detail Common
@endsection

@section('topbarTitle')
    Detail Common
@endsection

@section('content')
	<div class="row">
        <div class="col-lg-12">
            <div class="card-box">
			    @include('backend.commons.show_fields')

			    <div class="form-group">
			           <a href="{!! route('admin.commons.index') !!}" class="btn btn-default">Back</a>
			    </div>
		    </div>
	    </div>
	</div>
@endsection
