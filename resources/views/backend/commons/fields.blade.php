<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control','data-parsley-required' => 'true']) !!}
</div>

<!-- Option Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('option_value', 'Option Value:') !!}
    {!! Form::text('option_value', null, ['class' => 'form-control','data-parsley-required' => 'true']) !!}
</div>

<!-- Active Field -->
<div class="form-group col-sm-6">
    {!! Form::label('active', 'Active:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('active', 0) !!}
        {!! Form::checkbox('active', '1', null, ['data-plugin' => 'switchery']) !!}
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-right m-b-0">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.commons.index') !!}" class="btn btn-default waves-effect waves-light m-l-5">Cancel</a>
</div>

<div style="clear:both"></div>
