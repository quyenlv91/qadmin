<table class="table table-striped">
    <thead>
        <th>{!! Form::checkbox('all', '1', null, ['id' => 'chk-all']) !!}</th>
        <th>Name</th>
        <th>Option Value</th>
        <th>Active</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($commons as $common)
        <tr>
            <td>{!! Form::checkbox('common_ids[]', $common->id, null, ['class' => 'chk-ele']) !!}</td>
            <td>{!! $common->name !!}</td>
            <td>{!! $common->option_value !!}</td>
            <td style="text-align: center">{!! ($common->active == 1) ? '<i style="color: green;font-size: 1.3em" class="fa fa-check-circle-o"></i>' : '<i style="color: red;font-size: 1.3em" class="fa fa-circle-o"></i>' !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.commons.destroy', $common->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.commons.show', [$common->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.commons.edit', [$common->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'button', 'class' => 'btn btn-danger btn-xs sa-warning']) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
