<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control','data-parsley-required' => 'true']) !!}
</div>

<!-- Content Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', 'Content:') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control tinymce-editor']) !!}
</div>


<!-- Active Field -->
<div class="form-group col-sm-6">
    {!! Form::label('active', 'Active:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('active', 0) !!}
        {!! Form::checkbox('active', '1', null, ['data-plugin' => 'switchery']) !!}
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-right m-b-0">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.posts.index') !!}" class="btn btn-default waves-effect waves-light m-l-5">Cancel</a>
</div>

<div style="clear:both"></div>
