@extends('layouts.app')

@section('titleWS')
	{{ env('APP_NAME_UP') }} - Manage Posts
@endsection

@section('css')
	<!-- Editatable  Css-->
    <link rel="stylesheet" href="{{ asset('assets/plugins/magnific-popup/dist/magnific-popup.css') }}" />
    <!-- Sweet Alert css -->
    <link href="{{ asset('assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('javascript')
	<!-- Editable js -->
    <script src="{{ asset('assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js') }}"></script>
    <!-- Sweet Alert js -->
    <script src="{{ asset('assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
    <script src="{{ asset('assets/pages/jquery.sweet-alert.init.js') }}"></script>
@endsection

@section('javascript_end')
	<script type="text/javascript">
		$(document).ready(function() {
			$("#chk-all").click(function(){
				var isChecked = $(this).prop('checked');
				if(isChecked){
					$(".chk-ele").prop('checked', true);
				}
				else{
					$(".chk-ele").prop('checked', false);
				}
			});
			var chkEleLength = $(".chk-ele").length;
			$(".chk-ele").click(function(){
				var curLength = $(".chk-ele:checked").length;
				if(curLength == chkEleLength){
					$("#chk-all").prop('checked', true);
				}
				else{
					$("#chk-all").prop('checked', false);
				}
			});
		});
	</script>
@endsection

@section('topbarTitle')
	Posts
@endsection

@section('content')
	<div class="row">
	    <div class="col-sm-12">
	        <div class="panel">
	            <div class="panel-body">
	                <div class="row">
	                    <div class="col-sm-8">
	                        <div class="m-b-30">
	                        	<a href="{!! route('admin.posts.create') !!}">
	                            	<button id="addToTable" class="btn btn-primary waves-effect waves-light">Add <i class="fa fa-plus"></i></button>
	                            </a>
	                        </div>
	                    </div>

	                    <div class="col-sm-4">
	                        <div class="m-b-30">
	                        	{!! Form::open(['route' => 'admin.posts.index', 'method' => 'GET']) !!}
	                        		<div class="input-group">
                                        <span class="input-group-btn">
                                        <button type="submit" class="btn waves-effect waves-light btn-primary"><i class="fa fa-search"></i></button>
                                        </span>
                                        {!! Form::text('s', app('request')->input('s'), ['class' => 'form-control', 'placeholder' => 'Input text...']) !!}
                                    </div>
	                        	{!! Form::close() !!}
	                        </div>
	                    </div>
	                </div>

	                @if (app('request')->has('s') && strlen(trim(app('request')->input('s'))) > 0)
	                <div class="row">
		                <div class="col-sm-12">
		                	Search for "{{ app('request')->input('s') }}": {{ $posts->count() }} records.
		                </div>
	                </div>
	                @endif

	                <div class="editable-responsive">
	                    <div class="clearfix"></div>

						@include('flash::message')

						<div class="clearfix"></div>

						@include('backend.posts.table')
				        

				        @if (count($posts) > 0)
				        <div class="clearfix"></div>
					    {!! Form::open(['route' => 'admin.posts.group_action', 'method' => 'PUT']) !!}    
					        <div class="col-md-2">
					        	<select class="form-control" name="action">
					        		<option value="active">Active</option>
					        		<option value="deactive">Deactive</option>
					        		<option value="delete">Delete</option>
					        	</select>
					        </div>
					        <div class="col-md-2">
					        	<button id="btn-group-action" type="button" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">Action</button>
					        </div>
					    {!! Form::close() !!}
					    @endif
	                </div>
	            </div>
	            <!-- end: panel body -->

	        </div> <!-- end panel -->
	    </div> <!-- end col-->
	</div>
@endsection
