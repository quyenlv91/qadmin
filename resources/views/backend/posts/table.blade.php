<table class="table table-striped">
    <thead>
        <th>{!! Form::checkbox('all', '1', null, ['id' => 'chk-all']) !!}</th>
        <th>Title</th>
        <th>Content</th>
        <th>Active</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($posts as $post)
        <tr>
            <td>{!! Form::checkbox('post_ids[]', $post->id, null, ['class' => 'chk-ele']) !!}</td>
            <td>{!! $post->title !!}</td>
            <td>{!! $post->content !!}</td>
            <td style="text-align: center">{!! ($post->active == 1) ? '<i style="color: green;font-size: 1.3em" class="fa fa-check-circle-o"></i>' : '<i style="color: red;font-size: 1.3em" class="fa fa-circle-o"></i>' !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.posts.destroy', $post->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.posts.show', [$post->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.posts.edit', [$post->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'button', 'class' => 'btn btn-danger btn-xs sa-warning']) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
