@extends('layouts.app')

@section('titleWS')
	{{ env('APP_NAME_UP') }} - Detail Post
@endsection

@section('topbarTitle')
    Detail Post
@endsection

@section('content')
	<div class="row">
        <div class="col-lg-12">
            <div class="card-box">
			    @include('backend.posts.show_fields')

			    <div class="form-group">
			           <a href="{!! route('admin.posts.index') !!}" class="btn btn-default">Back</a>
			    </div>
		    </div>
	    </div>
	</div>
@endsection
