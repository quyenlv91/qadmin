@extends('layouts.app')

@section('css')
	<!-- Editatable  Css-->
    <link rel="stylesheet" href="{{ asset('assets/plugins/magnific-popup/dist/magnific-popup.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/plugins/jquery-datatables-editable/datatables.css') }}" />
    <!-- Sweet Alert css -->
    <link href="{{ asset('assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('javascript')
	<!-- Editable js -->
    <script src="{{ asset('assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-datatables-editable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>
    <!-- Sweet Alert js -->
    <script src="{{ asset('assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
    <script src="{{ asset('assets/pages/jquery.sweet-alert.init.js') }}"></script>
@endsection

@section('topbarTitle')
	Permissions
@endsection

@section('content')
	<div class="row">
	    <div class="col-sm-12">
	        <div class="panel">
	            <div class="panel-body">
	                <div class="row">
	                    <div class="col-sm-8">
	                        <div class="m-b-30">
	                        	<a href="{!! route('permissions.create') !!}">
	                            	<button id="addToTable" class="btn btn-primary waves-effect waves-light">Add <i class="fa fa-plus"></i></button>
	                            </a>
	                        </div>
	                    </div>

	                    <div class="col-sm-4">
	                        <div class="m-b-30">
	                        	{!! Form::open(['route' => 'permissions.index', 'method' => 'GET']) !!}
	                        		<div class="input-group">
                                        <span class="input-group-btn">
                                        <button type="submit" class="btn waves-effect waves-light btn-primary"><i class="fa fa-search"></i></button>
                                        </span>
                                        {!! Form::text('s', app('request')->input('s'), ['class' => 'form-control', 'placeholder' => 'Input text...']) !!}
                                    </div>
	                        	{!! Form::close() !!}
	                        </div>
	                    </div>
	                </div>

	                @if (app('request')->has('s') && strlen(trim(app('request')->input('s'))) > 0)
	                <div class="row">
		                <div class="col-sm-12">
		                	Search for "{{ app('request')->input('s') }}": {{ $permissions->count() }} records.
		                </div>
	                </div>
	                @endif

	                <div class="editable-responsive">
	                    <div class="clearfix"></div>

						@include('flash::message')

						<div class="clearfix"></div>

						@include('backend.permissions.table')
				        
        @include('core-templates::common.paginate', ['records' => $permissions])

	                </div>
	            </div>
	            <!-- end: panel body -->

	        </div> <!-- end panel -->
	    </div> <!-- end col-->
	</div>
@endsection
