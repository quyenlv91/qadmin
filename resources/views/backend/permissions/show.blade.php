@extends('layouts.app')

@section('topbarTitle')
    Detail Permissions
@endsection

@section('content')
	<div class="row">
        <div class="col-lg-12">
            <div class="card-box">
			    @include('backend.permissions.show_fields')

			    <div class="form-group">
			           <a href="{!! route('permissions.index') !!}" class="btn btn-default">Back</a>
			    </div>
		    </div>
	    </div>
	</div>
@endsection
