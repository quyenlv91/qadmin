<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control','data-parsley-required' => 'true']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control','data-parsley-required' => 'true']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password', ['class' => 'form-control','data-parsley-required' => 'true','data-parsley-equalto' => '#password_confirmation','data-parsley-minlength' => '6']) !!}
</div>

<!-- Password Confirmation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password_confirmation', 'Password Confirmation:') !!}
    {!! Form::password('password_confirmation', ['class' => 'form-control','data-parsley-required' => 'true','data-parsley-minlength' => '6']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('avatar', 'Avatar:') !!}
    {!! Form::file('avatar', ['class' => 'dropify']) !!}
</div>

<!-- Role Field -->
<div class="form-group col-sm-6">
    {!! Form::label('roles', 'Role: ') !!}
    {!! Form::select('roles[]', app('App\Repositories\Admin\RolesRepository')->all()->pluck('name', 'id')->toArray(), null, array('multiple' => true, 'class' => 'multi-select', 'id' => 'roles', 'data-plugin' => 'multiselect')) !!}
</div>

<!-- Active Field -->
<div class="form-group col-sm-6">
    {!! Form::label('active', 'Active:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('active', 0) !!}
        {!! Form::checkbox('active', '1', null, ['data-plugin' => 'switchery']) !!}
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-right m-b-0">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-default waves-effect waves-light m-l-5">Cancel</a>
</div>

<div style="clear:both"></div>
