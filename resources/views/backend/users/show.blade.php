@extends('layouts.app')

@section('topbarTitle')
    Detail Users
@endsection

@section('content')
	<div class="row">
        <div class="col-lg-12">
            <div class="card-box">
			    @include('backend.users.show_fields')

			    <div class="form-group">
			           <a href="{!! route('users.index') !!}" class="btn btn-default">Back</a>
			    </div>
		    </div>
	    </div>
	</div>
@endsection
