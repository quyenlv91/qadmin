<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control','data-parsley-required' => 'true']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control','data-parsley-required' => 'true']) !!}
</div>

<!-- Avatar Field -->
<div class="form-group col-sm-6">
    {!! Form::label('avatar', 'Avatar:') !!}
    {!! Form::file('avatar', ['class' => 'dropify']) !!}
</div>
<div class="form-group col-sm-6">
@if (isset($users) && $users->avatar != '')
    <label for="url">Old avatar:</label>
    <p><img src="{{ asset(config('image.avatar-path').$users->avatar) }}" width="100%"></p>
    <input type="hidden" name="old_avatar" value="{{ $users->avatar }}">
@endif
</div>

<!-- Role Field -->
<div class="form-group col-sm-6">
    {!! Form::label('roles', 'Role: ') !!}
    {!! Form::select('roles[]', app('App\Repositories\Admin\RolesRepository')->all()->pluck('name', 'id')->toArray(), isset($users) ? $users->roles->pluck('id')->toArray() : null, array('multiple' => true, 'class' => 'multi-select', 'id' => 'roles', 'data-plugin' => 'multiselect')) !!}
</div>

<!-- Active Field -->
<div class="form-group col-sm-6">
    {!! Form::label('active', 'Active:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('active', 0) !!}
        {!! Form::checkbox('active', '1', null, ['data-plugin' => 'switchery']) !!}
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-right m-b-0">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-default waves-effect waves-light m-l-5">Cancel</a>
</div>

<div style="clear:both"></div>
