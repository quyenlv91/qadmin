<table class="table table-striped" id="datatable-editable">
    <thead>
        <th>{!! Form::checkbox('all', '1', null, ['id' => 'chk-all']) !!}</th>
        <th>Name</th>
        <th>Email</th>
        <th style="text-align: center">Active</th>
        <th>Roles</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($users as $users)
        <tr>
            <td>{!! Form::checkbox('users_ids[]', $users->id, null, ['class' => 'chk-ele']) !!}</td>
            <td>{!! $users->name !!}</td>
            <td>{!! $users->email !!}</td>
            <td style="text-align: center">{!! ($users->active == 1) ? '<i style="color: green;font-size: 1.3em" class="fa fa-check-circle-o"></i>' : '<i style="color: red;font-size: 1.3em" class="fa fa-circle-o"></i>' !!}</td>
            <td>{!! $users->roles()->pluck('name')->implode(',') !!}</td>
            <td>
                {!! Form::open(['route' => ['users.destroy', $users->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('users.show', [$users->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('users.edit', [$users->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'button', 'class' => 'btn btn-danger btn-xs sa-warning']) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
