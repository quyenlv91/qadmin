<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control','data-parsley-required' => 'true']) !!}
</div>

<!-- Sub Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sub_title', 'Sub Title:') !!}
    {!! Form::text('sub_title', null, ['class' => 'form-control']) !!}
</div>
<div class="clearfix"></div>
<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image', ['class' => 'dropify']) !!}
</div>
<div class="form-group col-sm-6">
@if (isset($slides) && $slides->image != '')
    <label for="url">Old image:</label>
    <p><img src="{{ asset(config('image.slide-path').$slides->image) }}" width="100%"></p>
    <input type="hidden" name="old_image" value="{{ $slides->image }}">
@endif
</div>
<div class="clearfix"></div>

<!-- Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('url', 'Url:') !!}
    {!! Form::text('url', null, ['class' => 'form-control']) !!}
</div>

<!-- Order Field -->
<div class="form-group col-sm-2">
    {!! Form::label('order', 'Order:') !!}
    {!! Form::text('order', null, ['class' => 'form-control','data-parsley-type' => 'integer']) !!}
</div>

<!-- Active Field -->
<div class="form-group col-sm-6">
    {!! Form::label('active', 'Active:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('active', 0) !!}
        {!! Form::checkbox('active', '1', null, ['data-plugin' => 'switchery']) !!}
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-right m-b-0">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.slides.index') !!}" class="btn btn-default waves-effect waves-light m-l-5">Cancel</a>
</div>

<div style="clear:both"></div>
