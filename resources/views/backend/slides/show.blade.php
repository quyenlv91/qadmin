@extends('layouts.app')

@section('topbarTitle')
    Detail Slides
@endsection

@section('content')
	<div class="row">
        <div class="col-lg-12">
            <div class="card-box">
			    @include('backend.slides.show_fields')

			    <div class="form-group">
			           <a href="{!! route('admin.slides.index') !!}" class="btn btn-default">Back</a>
			    </div>
		    </div>
	    </div>
	</div>
@endsection
