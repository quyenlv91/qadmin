<table class="table table-striped">
    <thead>
        <th>{!! Form::checkbox('all', '1', null, ['id' => 'chk-all']) !!}</th>
        <th>Title</th>
        <th>Sub Title</th>
        <th>Image</th>
        <th>Order</th>
        <th style="text-align: center;">Active</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($slides as $slides)
        <tr>
            <td>{!! Form::checkbox('slides_ids[]', $slides->id, null, ['class' => 'chk-ele']) !!}</td>
            <td>{!! $slides->title !!}</td>
            <td>{!! $slides->sub_title !!}</td>
            <td>{!! $slides->image !!}</td>
            <td>{!! $slides->order !!}</td>
            <td style="text-align: center">{!! ($slides->active == 1) ? '<i style="color: green;font-size: 1.3em" class="fa fa-check-circle-o"></i>' : '<i style="color: red;font-size: 1.3em" class="fa fa-circle-o"></i>' !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.slides.destroy', $slides->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.slides.show', [$slides->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.slides.edit', [$slides->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'button', 'class' => 'btn btn-danger btn-xs sa-warning']) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
