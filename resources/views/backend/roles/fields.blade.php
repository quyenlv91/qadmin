<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control','data-parsley-required' => 'true']) !!}
</div>

<!-- Permissions Field -->
<div class="form-group col-sm-6">
    {!! Form::label('permissions', 'Permissions: ') !!}
    {!! Form::select('permissions[]', app('App\Repositories\Admin\PermissionsRepository')->all()->pluck('name', 'id')->toArray(), isset($roles) ? $roles->permissions->pluck('id')->toArray() : null, array('multiple' => true, 'class' => 'multi-select', 'id' => 'permissions', 'data-plugin' => 'multiselect')) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-right m-b-0">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('roles.index') !!}" class="btn btn-default waves-effect waves-light m-l-5">Cancel</a>
</div>

<div style="clear:both"></div>
