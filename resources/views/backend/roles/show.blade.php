@extends('layouts.app')

@section('topbarTitle')
    Detail Roles
@endsection

@section('content')
	<div class="row">
        <div class="col-lg-12">
            <div class="card-box">
			    @include('backend.roles.show_fields')

			    <div class="form-group">
			           <a href="{!! route('roles.index') !!}" class="btn btn-default">Back</a>
			    </div>
		    </div>
	    </div>
	</div>
@endsection
