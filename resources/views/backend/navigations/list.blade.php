<div class="col-sm-6">
    <ul id="sortable1" class="connectedSortable">
        @foreach ($navigations as $nav)
            @if ($nav->active)
                <li class="ui-state-default li-nav" data-id="{{ $nav->id }}">
                    <i class="{{ $nav->icon }}"></i> {{ $nav->title }}
                    {!! Form::open(['route' => ['navigations.destroy', $nav->id], 'method' => 'delete', 'class' => 'f-right']) !!}<span class="btn-action sa-warning">x</span>{!! Form::close() !!}
                    <a href="{!! route('navigations.edit', [$nav->id]) !!}" class='btn btn-default btn-xs f-right'><i class="glyphicon glyphicon-edit"></i></a>
                </li>
            @endif
        @endforeach
    </ul>
</div>

<div class="col-sm-6">
    <ul id="sortable2" class="connectedSortable">
        @foreach ($navigations as $nav)
            @if (!$nav->active)
                <li class="ui-state-highlight li-nav" data-id="{{ $nav->id }}">
                    <i class="{{ $nav->icon }}"></i> {{ $nav->title }}
                    {!! Form::open(['route' => ['navigations.destroy', $nav->id], 'method' => 'delete', 'class' => 'f-right']) !!}<span class="btn-action sa-warning">x</span>{!! Form::close() !!}
                    <a href="{!! route('navigations.edit', [$nav->id]) !!}" class='btn btn-default btn-xs f-right'><i class="glyphicon glyphicon-edit"></i></a>
                </li>
            @endif
        @endforeach
    </ul>
</div>

{!! Form::open(['url' => 'admin/navigations/save_orders', 'method' => 'post', 'id' => 'save-orders-frm']) !!}
    {!! Form::hidden('navigations_orders', null, ['id' => 'navigations-orders']) !!}
    {!! Form::hidden('navigations_deactive', null, ['id' => 'navigations-deactive']) !!}
{!! Form::close() !!}