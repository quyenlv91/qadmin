<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Image;
use JsonResponse;
use DB;

class UploadImageController extends AppBaseController
{
    public function index(Request $request)
    {
        $allFiles = $request->allFiles();
        if(count($allFiles) > 0){
            foreach ($allFiles as $k => $file) {
                $name = time().'_' . $file->getClientOriginalName();
                $img = Image::make($file->getPathname());
                $img->save(public_path(config('image.image-path') . $name));
                return response()->json(['files' => array([
                    "name" => $name,
                    "fieldName" => $k,
                    "size" => $file->getClientSize(),
                    "url" => asset(config('image.image-path').$name),
                    "thumbnailUrl" => asset(config('image.image-path').$name),
                    "deleteUrl" => url("admin/delete/images/".$name),
                    "deleteType" => "DELETE"
                ])]);
            }
        }
        $error = array(
          	'error' => 'Upload error!',
      	);
      	// Return error
      	return Response::json($error, 400);
    }

    public function delete($image_name)
    {
    	if(!empty($image_name) && file_exists(public_path(config('image.image-path') . $image_name))){
            unlink(public_path(config('image.image-path') . $image_name));
            return response()->json(['url' => false]);
        }
    }

    public function get_images($table, $field, $id)
    {
        $images = DB::table($table)->select($field)->find($id);
        if(empty($images)){
            return response()->json(['files' => []]);
        }
        $imagesArr = explode(',', $images->{$field});
        $fileArr = array();
        foreach ($imagesArr as $name) {
            $fileArr[] = [
                    "name" => $name,
                    "size" => 'undefine',
                    "fieldName" => $field,
                    "url" => asset(config('image.image-path').$name),
                    "thumbnailUrl" => asset(config('image.image-path').$name),
                    "deleteUrl" => url("admin/delete/images/".$name),
                    "deleteType" => "DELETE"
                ];
        }
        return response()->json(['files' => $fileArr]);
    }
}
