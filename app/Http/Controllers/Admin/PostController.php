<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreatePostRequest;
use App\Http\Requests\Admin\UpdatePostRequest;
use App\Repositories\Admin\PostRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;

class PostController extends AppBaseController
{
    /** @var  PostRepository */
    private $postRepository;

    public function __construct(PostRepository $postRepo)
    {
        $this->postRepository = $postRepo;
        $this->middleware('admin.check');$this->middleware('role:Manage Post');
    }

    /**
     * Display a listing of the Post.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->postRepository->pushCriteria(new RequestCriteria($request));
        $this->postRepository->applySearch();
        $posts = $this->postRepository->all();

        return view('backend.posts.index')
            ->with('posts', $posts);
    }

    /**
     * Show the form for creating a new Post.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.posts.create');
    }

    /**
     * Store a newly created Post in storage.
     *
     * @param CreatePostRequest $request
     *
     * @return Response
     */
    public function store(CreatePostRequest $request)
    {
        $input = $request->all();
        
        $post = $this->postRepository->create($input);

        Flash::success('Post saved successfully.');

        return redirect(route('admin.posts.index'));
    }

    /**
     * Display the specified Post.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $post = $this->postRepository->findWithoutFail($id);

        if (empty($post)) {
            Flash::error('Post not found');

            return redirect(route('admin.posts.index'));
        }

        return view('backend.posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified Post.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $post = $this->postRepository->findWithoutFail($id);

        if (empty($post)) {
            Flash::error('Post not found');

            return redirect(route('admin.posts.index'));
        }

        return view('backend.posts.edit')->with('post', $post);
    }

    /**
     * Update the specified Post in storage.
     *
     * @param  int              $id
     * @param UpdatePostRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePostRequest $request)
    {
        $post = $this->postRepository->findWithoutFail($id);

        if (empty($post)) {
            Flash::error('Post not found');

            return redirect(route('admin.posts.index'));
        }
        $input = $request->all();
        
        $post = $this->postRepository->update($input, $id);

        Flash::success('Post updated successfully.');

        return redirect(route('admin.posts.index'));
    }

    /**
     * Remove the specified Post from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $post = $this->postRepository->findWithoutFail($id);

        if (empty($post)) {
            Flash::error('Post not found');

            return redirect(route('admin.posts.index'));
        }
        
        $this->postRepository->delete($id);

        Flash::success('Post deleted successfully.');

        return redirect(route('admin.posts.index'));
    }

    /**
     * Action with multi records
     * @param  Request $request
     */
    public function group_action(Request $request)
    {
        $actions = array('active', 'deactive', 'delete');
        if(!in_array($request->get('action'), $actions))
        {
            Flash::error('Action not define');
            return redirect(route('admin.posts.index'));
        }
        $idArr = $request->get('posts_ids');
        if(count($idArr) == 0)
        {
            Flash::error('Post not found');
            return redirect(route('admin.posts.index'));
        }
        $statusAction = false;
        if($request->get('action') == 'delete'){
            $statusAction = $this->multi_delete($idArr);
        }
        else
        {
            $statusAction = $this->multi_change_active($idArr, $request->get('action'));
        }
        if($statusAction)
        {
            Flash::success('Action success');
        }
        else{
            Flash::error('Error exception!');
        }
        return redirect(route('admin.posts.index'));
    }

    /**
     * Remove the specified multi Post from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    private function multi_delete($idArr)
    {
        foreach ($idArr as $id) {
            $posts = $this->postsRepository->findWithoutFail($id);

            if (empty($posts)) {
                Flash::error('Post not found');
                return redirect(route('admin.posts.index'));
            }

            $this->postsRepository->delete($id);
        }
        return true;
    }

    /**
     * Change active field for multi record
     * @param  [array]  $idArr  
     * @param  [string] $action
     */
    private function multi_change_active($idArr, $action)
    {
        if($action == 'active'){
            $data = 1;
        }
        elseif($action == 'deactive')
        {
            $data = 0;
        }
        else
        {
            Flash::error('Post not action');
            return redirect(route('admin.posts.index'));
        }
        DB::table('posts')->whereIn('id', $idArr)->update(['active' => $data]);
        return true;
    }
}
