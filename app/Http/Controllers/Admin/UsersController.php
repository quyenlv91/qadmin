<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use App\Repositories\Admin\UsersRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Admin\Users;
use DB;
use Image;

class UsersController extends AppBaseController
{
    /** @var  UsersRepository */
    private $usersRepository;

    public function __construct(UsersRepository $usersRepo)
    {
        $this->usersRepository = $usersRepo;
        $this->middleware('role:Manage Users');
    }

    /**
     * Display a listing of the Users.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->usersRepository->pushCriteria(new RequestCriteria($request));
        $this->usersRepository->applySearch();
        $users = $this->usersRepository->paginate(10);

        return view('backend.users.index')
            ->with('users', $users);
    }

    /**
     * Show the form for creating a new Users.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.users.create');
    }

    /**
     * Store a newly created Users in storage.
     *
     * @param CreateUsersRequest $request
     *
     * @return Response
     */
    public function store(CreateUsersRequest $request)
    {
        $input = $request->all();
        if($request->hasFile('avatar'))
        {
            $file = $request->file('avatar');
            $input['avatar'] = time().'_' . $file->getClientOriginalName();
            $img = Image::make($file->getPathname());
            $img->fit(200)->save(public_path(config('image.avatar-path') . $input['avatar']));
        }

        $users = $this->usersRepository->create($input);

        Flash::success('Users saved successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Display the specified Users.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $users = $this->usersRepository->findWithoutFail($id);

        if (empty($users)) {
            Flash::error('Users not found');

            return redirect(route('users.index'));
        }

        return view('backend.users.show')->with('users', $users);
    }

    /**
     * Show the form for editing the specified Users.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $users = $this->usersRepository->findWithoutFail($id);
        if (empty($users)) {
            Flash::error('Users not found');

            return redirect(route('users.index'));
        }
        return view('backend.users.edit')->with('users', $users);
    }

    /**
     * Update the specified Users in storage.
     *
     * @param  int              $id
     * @param UpdateUsersRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUsersRequest $request)
    {
        $users = $this->usersRepository->findWithoutFail($id);

        if (empty($users)) {
            Flash::error('Users not found');

            return redirect(route('users.index'));
        }
        $input = $request->all();
        if($request->hasFile('avatar'))
        {
            $file = $request->file('avatar');
            $input['avatar'] = time().'_' . $file->getClientOriginalName();
            $img = Image::make($file->getPathname());
            $img->fit(200)->save(public_path(config('image.avatar-path') . $input['avatar']));
            if(!empty($input['old_avatar']) && file_exists(public_path(config('image.avatar-path') . $input['old_avatar']))){
                unlink(public_path(config('image.avatar-path') . $input['old_avatar']));
            }
        }
        else{
            $input['avatar'] = $input['old_avatar'];
        }
        unset($input['old_avatar']);

        $users = $this->usersRepository->update($input, $id);

        Flash::success('Users updated successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified Users from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $users = $this->usersRepository->findWithoutFail($id);

        if (empty($users)) {
            Flash::error('Users not found');

            return redirect(route('users.index'));
        }

        if(!empty($users->avatar) && file_exists(public_path(config('image.avatar-path') . $users->avatar))){
                    unlink(public_path(config('image.avatar-path') . $users->avatar));
                }

        $this->usersRepository->delete($id);

        Flash::success('Users deleted successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Action with multi records
     * @param  Request $request
     */
    public function group_action(Request $request)
    {
        $actions = array('active', 'deactive', 'delete');
        if(!in_array($request->get('action'), $actions))
        {
            Flash::error('Action not define');
            return redirect(route('users.index'));
        }
        $idArr = $request->get('users_ids');
        if(count($idArr) == 0)
        {
            Flash::error('Users not found');
            return redirect(route('users.index'));
        }
        $statusAction = false;
        if($request->get('action') == 'delete'){
            $statusAction = $this->multi_delete($idArr);
        }
        else
        {
            $statusAction = $this->multi_change_active($idArr, $request->get('action'));
        }
        if($statusAction)
        {
            Flash::success('Action success');
        }
        else{
            Flash::error('Error exception!');
        }
        return redirect(route('users.index'));
    }

    /**
     * Remove the specified multi Users from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    private function multi_delete($idArr)
    {
        foreach ($idArr as $id) {
            $users = $this->usersRepository->findWithoutFail($id);

            if (empty($users)) {
                Flash::error('Users not found');
                return redirect(route('users.index'));
            }

            if(!empty($users->avatar) && file_exists(public_path(config('image.avatar-path') . $users->avatar))){
                    unlink(public_path(config('image.avatar-path') . $users->avatar));
                }

            $this->usersRepository->delete($id);
        }
        return true;
    }

    /**
     * Change active field for multi record
     * @param  [array]  $idArr  
     * @param  [string] $action
     */
    private function multi_change_active($idArr, $action)
    {
        if($action == 'active'){
            $data = 1;
        }
        elseif($action == 'deactive')
        {
            $data = 0;
        }
        else
        {
            Flash::error('Users not action');
            return redirect(route('users.index'));
        }
        DB::table('users')->whereIn('id', $idArr)->update(['active' => $data]);
        return true;
    }
}
