<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateNavigationsRequest;
use App\Http\Requests\Admin\UpdateNavigationsRequest;
use App\Repositories\Admin\NavigationsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Admin\Navigations;

class NavigationsController extends AppBaseController
{
    /** @var  NavigationsRepository */
    private $navigationsRepository;

    public function __construct(NavigationsRepository $navigationsRepo)
    {
        $this->navigationsRepository = $navigationsRepo;
        $this->middleware('role:Manage Navigations');
    }

    /**
     * Display a listing of the Navigations.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->navigationsRepository->pushCriteria(new RequestCriteria($request));
        // $this->navigationsRepository->applySearch();
        $navigations = $this->navigationsRepository->all();

        return view('backend.navigations.index')
            ->with('navigations', $navigations);
    }

    /**
     * Show the form for creating a new Navigations.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.navigations.create');
    }

    /**
     * Store a newly created Navigations in storage.
     *
     * @param CreateNavigationsRequest $request
     *
     * @return Response
     */
    public function store(CreateNavigationsRequest $request)
    {
        $input = $request->all();

        $navigations = $this->navigationsRepository->create($input);

        Flash::success('Navigations saved successfully.');

        return redirect(route('navigations.index'));
    }

    /**
     * Display the specified Navigations.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $navigations = $this->navigationsRepository->findWithoutFail($id);

        if (empty($navigations)) {
            Flash::error('Navigations not found');

            return redirect(route('navigations.index'));
        }

        return view('backend.navigations.show')->with('navigations', $navigations);
    }

    /**
     * Show the form for editing the specified Navigations.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $navigations = $this->navigationsRepository->findWithoutFail($id);

        if (empty($navigations)) {
            Flash::error('Navigations not found');

            return redirect(route('navigations.index'));
        }

        return view('backend.navigations.edit')->with('navigations', $navigations);
    }

    /**
     * Update the specified Navigations in storage.
     *
     * @param  int              $id
     * @param UpdateNavigationsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNavigationsRequest $request)
    {
        $navigations = $this->navigationsRepository->findWithoutFail($id);

        if (empty($navigations)) {
            Flash::error('Navigations not found');

            return redirect(route('admin.navigations.index'));
        }

        $navigations = $this->navigationsRepository->update($request->all(), $id);

        Flash::success('Navigations updated successfully.');

        return redirect(route('navigations.index'));
    }

    /**
     * Remove the specified Navigations from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $navigations = $this->navigationsRepository->findWithoutFail($id);

        if (empty($navigations)) {
            Flash::error('Navigations not found');

            return redirect(route('navigations.index'));
        }

        $this->navigationsRepository->delete($id);

        Flash::success('Navigations deleted successfully.');

        return redirect(route('navigations.index'));
    }

    /**
     * save Order Navigations
     * @return Response
     */
    public function saveOrders()
    {
        $data = app('request')->only(['navigations_orders', 'navigations_deactive']);
        $data['navigations_orders'] = explode(',', $data['navigations_orders']);
        if(count($data['navigations_orders']) > 0)
        {
            foreach ($data['navigations_orders'] as $k => $navOrderId) {
                Navigations::where('id', '=', $navOrderId)->update(['active' => 1, 'order' => $k + 1]);
            }
        }
        $data['navigations_deactive'] = explode(',', $data['navigations_deactive']);
        if(count($data['navigations_deactive']) > 0)
        {
            foreach ($data['navigations_deactive'] as $k => $navDeactiveId) {
                Navigations::where('id', '=', $navDeactiveId)->update(['active' => 0]);
            }
        }
        Navigations::updateNavigationLayout();
        Flash::success('Navigations updated successfully.');
        return redirect(route('navigations.index'));
    }
}
