<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateCommonRequest;
use App\Http\Requests\Admin\UpdateCommonRequest;
use App\Repositories\Admin\CommonRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;

class CommonController extends AppBaseController
{
    /** @var  CommonRepository */
    private $commonRepository;

    public function __construct(CommonRepository $commonRepo)
    {
        $this->commonRepository = $commonRepo;
        $this->middleware('admin.check');$this->middleware('role:Manage Common');
    }

    /**
     * Display a listing of the Common.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->commonRepository->pushCriteria(new RequestCriteria($request));
        $this->commonRepository->applySearch();
        $commons = $this->commonRepository->all();

        return view('backend.commons.index')
            ->with('commons', $commons);
    }

    /**
     * Show the form for creating a new Common.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.commons.create');
    }

    /**
     * Store a newly created Common in storage.
     *
     * @param CreateCommonRequest $request
     *
     * @return Response
     */
    public function store(CreateCommonRequest $request)
    {
        $input = $request->all();
        
        $common = $this->commonRepository->create($input);

        Flash::success('Common saved successfully.');

        return redirect(route('admin.commons.index'));
    }

    /**
     * Display the specified Common.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $common = $this->commonRepository->findWithoutFail($id);

        if (empty($common)) {
            Flash::error('Common not found');

            return redirect(route('admin.commons.index'));
        }

        return view('backend.commons.show')->with('common', $common);
    }

    /**
     * Show the form for editing the specified Common.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $common = $this->commonRepository->findWithoutFail($id);

        if (empty($common)) {
            Flash::error('Common not found');

            return redirect(route('admin.commons.index'));
        }

        return view('backend.commons.edit')->with('common', $common);
    }

    /**
     * Update the specified Common in storage.
     *
     * @param  int              $id
     * @param UpdateCommonRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCommonRequest $request)
    {
        $common = $this->commonRepository->findWithoutFail($id);

        if (empty($common)) {
            Flash::error('Common not found');

            return redirect(route('admin.commons.index'));
        }
        $input = $request->all();
        
        $common = $this->commonRepository->update($input, $id);

        Flash::success('Common updated successfully.');

        return redirect(route('admin.commons.index'));
    }

    /**
     * Remove the specified Common from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $common = $this->commonRepository->findWithoutFail($id);

        if (empty($common)) {
            Flash::error('Common not found');

            return redirect(route('admin.commons.index'));
        }
        
        $this->commonRepository->delete($id);

        Flash::success('Common deleted successfully.');

        return redirect(route('admin.commons.index'));
    }

    /**
     * Action with multi records
     * @param  Request $request
     */
    public function group_action(Request $request)
    {
        $actions = array('active', 'deactive', 'delete');
        if(!in_array($request->get('action'), $actions))
        {
            Flash::error('Action not define');
            return redirect(route('admin.commons.index'));
        }
        $idArr = $request->get('commons_ids');
        if(count($idArr) == 0)
        {
            Flash::error('Common not found');
            return redirect(route('admin.commons.index'));
        }
        $statusAction = false;
        if($request->get('action') == 'delete'){
            $statusAction = $this->multi_delete($idArr);
        }
        else
        {
            $statusAction = $this->multi_change_active($idArr, $request->get('action'));
        }
        if($statusAction)
        {
            Flash::success('Action success');
        }
        else{
            Flash::error('Error exception!');
        }
        return redirect(route('admin.commons.index'));
    }

    /**
     * Remove the specified multi Common from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    private function multi_delete($idArr)
    {
        foreach ($idArr as $id) {
            $commons = $this->commonsRepository->findWithoutFail($id);

            if (empty($commons)) {
                Flash::error('Common not found');
                return redirect(route('admin.commons.index'));
            }

            $this->commonsRepository->delete($id);
        }
        return true;
    }

    /**
     * Change active field for multi record
     * @param  [array]  $idArr  
     * @param  [string] $action
     */
    private function multi_change_active($idArr, $action)
    {
        if($action == 'active'){
            $data = 1;
        }
        elseif($action == 'deactive')
        {
            $data = 0;
        }
        else
        {
            Flash::error('Common not action');
            return redirect(route('admin.commons.index'));
        }
        DB::table('commons')->whereIn('id', $idArr)->update(['active' => $data]);
        return true;
    }
}
