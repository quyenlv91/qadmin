<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateSlidesRequest;
use App\Http\Requests\Admin\UpdateSlidesRequest;
use App\Repositories\Admin\SlidesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Image;
use DB;

class SlidesController extends AppBaseController
{
    /** @var  SlidesRepository */
    private $slidesRepository;

    public function __construct(SlidesRepository $slidesRepo)
    {
        $this->slidesRepository = $slidesRepo;
        $this->middleware('admin.check');$this->middleware('role:Manage Slides');
    }

    /**
     * Display a listing of the Slides.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->slidesRepository->pushCriteria(new RequestCriteria($request));
        $this->slidesRepository->applySearch();
        $slides = $this->slidesRepository->all();

        return view('backend.slides.index')
            ->with('slides', $slides);
    }

    /**
     * Show the form for creating a new Slides.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.slides.create');
    }

    /**
     * Store a newly created Slides in storage.
     *
     * @param CreateSlidesRequest $request
     *
     * @return Response
     */
    public function store(CreateSlidesRequest $request)
    {
        $input = $request->all();
        if($request->hasFile('image'))
        {
            $file = $request->file('image');
            $input['image'] = time().'_' . $file->getClientOriginalName();
            $img = Image::make($file->getPathname());
            $img->save(public_path(config('image.slide-path') . $input['image']));
        }

        $slides = $this->slidesRepository->create($input);

        Flash::success('Slides saved successfully.');

        return redirect(route('admin.slides.index'));
    }

    /**
     * Display the specified Slides.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $slides = $this->slidesRepository->findWithoutFail($id);

        if (empty($slides)) {
            Flash::error('Slides not found');

            return redirect(route('admin.slides.index'));
        }

        return view('backend.slides.show')->with('slides', $slides);
    }

    /**
     * Show the form for editing the specified Slides.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $slides = $this->slidesRepository->findWithoutFail($id);

        if (empty($slides)) {
            Flash::error('Slides not found');

            return redirect(route('admin.slides.index'));
        }

        return view('backend.slides.edit')->with('slides', $slides);
    }

    /**
     * Update the specified Slides in storage.
     *
     * @param  int              $id
     * @param UpdateSlidesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSlidesRequest $request)
    {
        $slides = $this->slidesRepository->findWithoutFail($id);

        if (empty($slides)) {
            Flash::error('Slides not found');

            return redirect(route('admin.slides.index'));
        }
        $input = $request->all();
        if($request->hasFile('image'))
        {
            $file = $request->file('image');
            $input['image'] = time().'_' . $file->getClientOriginalName();
            $img = Image::make($file->getPathname());
            $img->save(public_path(config('image.slide-path') . $input['image']));
            if(!empty($input['old_image']) && file_exists(public_path(config('image.slide-path') . $input['old_image']))){
                unlink(public_path(config('image.slide-path') . $input['old_image']));
            }
        }
        else{
            $input['image'] = $input['old_image'];
        }
        unset($input['old_image']);

        $slides = $this->slidesRepository->update($input, $id);

        Flash::success('Slides updated successfully.');

        return redirect(route('admin.slides.index'));
    }

    /**
     * Remove the specified Slides from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $slides = $this->slidesRepository->findWithoutFail($id);

        if (empty($slides)) {
            Flash::error('Slides not found');

            return redirect(route('admin.slides.index'));
        }

        if(!empty($slides->image) && file_exists(public_path(config('image.slide-path') . $slides->image))){
                unlink(public_path(config('image.slide-path') . $slides->image));
            }

        $this->slidesRepository->delete($id);

        Flash::success('Slides deleted successfully.');

        return redirect(route('admin.slides.index'));
    }

    /**
     * Action with multi records
     * @param  Request $request
     */
    public function group_action(Request $request)
    {
        $actions = array('active', 'deactive', 'delete');
        if(!in_array($request->get('action'), $actions))
        {
            Flash::error('Action not define');
            return redirect(route('admin.slides.index'));
        }
        $idArr = $request->get('slides_ids');
        if(count($idArr) == 0)
        {
            Flash::error('Slides not found');
            return redirect(route('admin.slides.index'));
        }
        $statusAction = false;
        if($request->get('action') == 'delete'){
            $statusAction = $this->multi_delete($idArr);
        }
        else
        {
            $statusAction = $this->multi_change_active($idArr, $request->get('action'));
        }
        if($statusAction)
        {
            Flash::success('Action success');
        }
        else{
            Flash::error('Error exception!');
        }
        return redirect(route('admin.slides.index'));
    }

    /**
     * Remove the specified multi Slides from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    private function multi_delete($idArr)
    {
        foreach ($idArr as $id) {
            $slides = $this->slidesRepository->findWithoutFail($id);

            if (empty($slides)) {
                Flash::error('Slides not found');
                return redirect(route('admin.slides.index'));
            }

            if(!empty($slides->image) && file_exists(public_path(config('image.slide-path') . $slides->image))){
                    unlink(public_path(config('image.slide-path') . $slides->image));
                }

            $this->slidesRepository->delete($id);
        }
        return true;
    }

    /**
     * Change active field for multi record
     * @param  [array]  $idArr  
     * @param  [string] $action
     */
    private function multi_change_active($idArr, $action)
    {
        if($action == 'active'){
            $data = 1;
        }
        elseif($action == 'deactive')
        {
            $data = 0;
        }
        else
        {
            Flash::error('Slides not action');
            return redirect(route('admin.slides.index'));
        }
        DB::table('slides')->whereIn('id', $idArr)->update(['active' => $data]);
        return true;
    }
}
