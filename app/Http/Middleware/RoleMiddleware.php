<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        if (!Auth::guard('admin')->check()) {
            return redirect('/');
        }

        $admin = Auth::guard('admin')->user();
        if (!$admin->hasPermission($permission))
        {
            abort(503);
        }

        return $next($request);
    }
}
