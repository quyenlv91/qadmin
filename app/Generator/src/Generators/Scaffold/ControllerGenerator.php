<?php

namespace InfyOm\Generator\Generators\Scaffold;

use InfyOm\Generator\Common\CommandData;
use InfyOm\Generator\Generators\BaseGenerator;
use InfyOm\Generator\Utils\FileUtil;
use Log;

class ControllerGenerator extends BaseGenerator
{
    /** @var CommandData */
    private $commandData;

    /** @var string */
    private $path;

    /** @var string */
    private $templateType;

    /** @var string */
    private $fileName;

    public function __construct(CommandData $commandData)
    {
        parent::__construct($commandData);
        $this->commandData = $commandData;
        $this->path = $commandData->config->pathController;
        $this->templateType = config('infyom.laravel_generator.templates', 'core-templates');
        $this->fileName = $this->commandData->modelName.'Controller.php';
    }

    public function generate()
    {
        if ($this->commandData->getAddOn('datatables')) {
            $templateData = get_template('scaffold.controller.datatable_controller', 'laravel-generator');

            $this->generateDataTable();
        } else {
            $templateData = $this->isImage ? get_template('scaffold.controller.image_controller', 'laravel-generator') : get_template('scaffold.controller.controller', 'laravel-generator');

            $paginate = $this->commandData->getOption('paginate');

            if ($paginate) {
                $templateData = str_replace('$RENDER_TYPE$', 'paginate('.$paginate.')', $templateData);
            } else {
                $templateData = str_replace('$RENDER_TYPE$', 'all()', $templateData);
            }

            if($this->isMultiImage){
                $multiDestroy = '$teArr = explode(\',\', $$MODEL_NAME_CAMEL$->'.$this->fieldMulti.');
                                    if(count($teArr) > 0){
                                        foreach ($teArr as $temp) {
                                            if(!empty($temp) && file_exists(public_path(config(\'image.image-path\') . $temp))){
                                                unlink(public_path(config(\'image.image-path\') . $temp));
                                            }
                                        }
                                    }';
                $templateData = str_replace('$MULTI_IMAGES$', '$input[\''.$this->fieldMulti.'\'] = isset($input[\''.$this->fieldMulti.'\']) ? implode(\',\', $input[\''.$this->fieldMulti.'\']) : \'\';', $templateData);
                $templateData = str_replace('$MULTI_DESTROY$', $multiDestroy, $templateData);
            }
            else{
                $templateData = str_replace('$MULTI_IMAGES$', '', $templateData);
                $templateData = str_replace('$MULTI_DESTROY$', '', $templateData);
            }
        }

        // replace middleware check if is Admin controller.
        if($this->commandData->getOption('prefix') == 'admin')
        {
            $templateData = str_replace('$ADMIN_CHECK_MIDDLEWARE$', '$this->middleware(\'admin.check\');$this->middleware(\'role:Manage ' . $this->commandData->modelName . '\');', $templateData);
        }
        else
        {
            $templateData = str_replace('$ADMIN_CHECK_MIDDLEWARE$', '', $templateData);
        }
        
        $imageFields = '';
        if($this->isImage)
        {
            if(count($this->imageFields) == 1){
                $imageFields = '$imageFields = array("' . $this->imageFields[0] . '");';
            }
            else{
                $imageFields = '$imageFields = array("' . implode('","', $this->imageFields) . '");';
            }
        }

        $templateData = str_replace('$IMAGE_FIELDS$', $imageFields, $templateData);


        $templateData = fill_template($this->commandData->dynamicVars, $templateData);

        FileUtil::createFile($this->path, $this->fileName, $templateData);

        $this->commandData->commandComment("\nController created: ");
        $this->commandData->commandInfo($this->fileName);
    }

    private function generateDataTable()
    {
        $templateData = get_template('scaffold.datatable', 'laravel-generator');

        $templateData = fill_template($this->commandData->dynamicVars, $templateData);

        $headerFieldTemplate = get_template('scaffold.views.datatable_column', $this->templateType);

        $headerFields = [];

        foreach ($this->commandData->fields as $field) {
            if (!$field->inIndex) {
                continue;
            }
            $headerFields[] = $fieldTemplate = fill_template_with_field_data(
                $this->commandData->dynamicVars,
                $this->commandData->fieldNamesMapping,
                $headerFieldTemplate,
                $field
            );
        }

        $path = $this->commandData->config->pathDataTables;

        $fileName = $this->commandData->modelName.'DataTable.php';

        $fields = implode(','.infy_nl_tab(1, 3), $headerFields);

        $templateData = str_replace('$DATATABLE_COLUMNS$', $fields, $templateData);

        FileUtil::createFile($path, $fileName, $templateData);

        $this->commandData->commandComment("\n$fileName created: ");
        $this->commandData->commandInfo($fileName);
    }

    public function rollback()
    {
        if ($this->rollbackFile($this->path, $this->fileName)) {
            $this->commandData->commandComment('Controller file deleted: '.$this->fileName);
        }
    }
}
