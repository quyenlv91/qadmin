<?php

namespace InfyOm\Generator\Generators\Scaffold;

use Illuminate\Support\Str;
use InfyOm\Generator\Common\CommandData;
use InfyOm\Generator\Generators\BaseGenerator;
use InfyOm\Generator\Utils\FileUtil;
use InfyOm\Generator\Utils\GeneratorFieldsInputUtil;
use InfyOm\Generator\Utils\HTMLFieldGenerator;
use Log;

class ViewGenerator extends BaseGenerator
{
    /** @var CommandData */
    private $commandData;

    /** @var string */
    private $path;

    /** @var string */
    private $templateType;

    /** @var array */
    private $htmlFields;

    public function __construct(CommandData $commandData)
    {
        parent::__construct($commandData);
        if($commandData->getOption('prefix') == 'admin')
        {
            $commandData->addDynamicVariable('$VIEW_PREFIX$', 'backend.');
        }
        $this->commandData = $commandData;
        $this->path = $commandData->config->pathViews;
        $this->templateType = config('infyom.laravel_generator.templates', 'core-templates');
    }

    public function generate()
    {
        if (!file_exists($this->path)) {
            mkdir($this->path, 0755, true);
        }

        $this->commandData->commandComment("\nGenerating Views...");

        if ($this->commandData->getOption('views')) {
            $viewsToBeGenerated = explode(',', $this->commandData->getOption('views'));

            if (in_array('index', $viewsToBeGenerated)) {
                $this->generateTable();
                $this->generateIndex();
            }

            if (count(array_intersect(['create', 'update'], $viewsToBeGenerated)) > 0) {
                $this->generateFields();
            }

            if (in_array('create', $viewsToBeGenerated)) {
                $this->generateCreate();
            }

            if (in_array('edit', $viewsToBeGenerated)) {
                $this->generateUpdate();
            }

            if (in_array('show', $viewsToBeGenerated)) {
                $this->generateShowFields();
                $this->generateShow();
            }
        } else {
            $this->generateTable();
            $this->generateIndex();
            $this->generateFields();
            $this->generateCreate();
            $this->generateUpdate();
            $this->generateShowFields();
            $this->generateShow();
        }

        $this->commandData->commandComment('Views created: ');
    }

    private function generateTable()
    {
        if ($this->commandData->getAddOn('datatables')) {
            $templateData = $this->generateDataTableBody();
            $this->generateDataTableActions();
        } else {
            $templateData = $this->generateBladeTableBody();
        }

        FileUtil::createFile($this->path, 'table.blade.php', $templateData);

        $this->commandData->commandInfo('table.blade.php created');
    }

    private function generateDataTableBody()
    {
        $templateData = get_template('scaffold.views.datatable_body', $this->templateType);

        return fill_template($this->commandData->dynamicVars, $templateData);
    }

    private function generateDataTableActions()
    {
        $templateData = get_template('scaffold.views.datatables_actions', $this->templateType);

        $templateData = fill_template($this->commandData->dynamicVars, $templateData);

        FileUtil::createFile($this->path, 'datatables_actions.blade.php', $templateData);

        $this->commandData->commandInfo('datatables_actions.blade.php created');
    }

    private function generateBladeTableBody()
    {
        $templateData = get_template('scaffold.views.blade_table_body', $this->templateType);

        $templateData = fill_template($this->commandData->dynamicVars, $templateData);

        $templateData = str_replace('$FIELD_HEADERS$', $this->generateTableHeaderFields(), $templateData);

        $cellFieldTemplate = get_template('scaffold.views.table_cell', $this->templateType);
        $cellActiveFieldTemplate = get_template('scaffold.views.table_active_cell', $this->templateType);

        $tableBodyFields = [];

        foreach ($this->commandData->fields as $field) {
            if (!$field->inIndex) {
                continue;
            }
            if($field->name == 'active'){
                $tableBodyFields[] = fill_template_with_field_data(
                    $this->commandData->dynamicVars,
                    $this->commandData->fieldNamesMapping,
                    $cellActiveFieldTemplate,
                    $field
                );
                continue;
            }
            $tableBodyFields[] = fill_template_with_field_data(
                $this->commandData->dynamicVars,
                $this->commandData->fieldNamesMapping,
                $cellFieldTemplate,
                $field
            );
        }

        $tableBodyFields = implode(infy_nl_tab(1, 3), $tableBodyFields);

        return str_replace('$FIELD_BODY$', $tableBodyFields, $templateData);
    }

    private function generateTableHeaderFields()
    {
        $headerFieldTemplate = get_template('scaffold.views.table_header', $this->templateType);

        $headerFields = [];

        foreach ($this->commandData->fields as $field) {
            if (!$field->inIndex) {
                continue;
            }
            $headerFields[] = $fieldTemplate = fill_template_with_field_data(
                $this->commandData->dynamicVars,
                $this->commandData->fieldNamesMapping,
                $headerFieldTemplate,
                $field
            );
        }

        return implode(infy_nl_tab(1, 2), $headerFields);
    }

    private function generateIndex()
    {
        $templateData = get_template('scaffold.views.index', $this->templateType);

        $templateData = fill_template($this->commandData->dynamicVars, $templateData);

        if ($this->commandData->getOption('datatables')) {
            $templateData = str_replace('$PAGINATE$', '', $templateData);
        } else {
            $paginate = $this->commandData->getOption('paginate');

            if ($paginate) {
                $paginateTemplate = get_template('scaffold.views.paginate', $this->templateType);

                $paginateTemplate = fill_template($this->commandData->dynamicVars, $paginateTemplate);

                $templateData = str_replace('$PAGINATE$', $paginateTemplate, $templateData);
            } else {
                $templateData = str_replace('$PAGINATE$', '', $templateData);
            }
        }

        FileUtil::createFile($this->path, 'index.blade.php', $templateData);

        $this->commandData->commandInfo('index.blade.php created');
    }

    private function generateFields()
    {
        $this->htmlFields = [];

        foreach ($this->commandData->fields as $field) {
            if (!$field->inForm) {
                continue;
            }

            $fieldTemplate = HTMLFieldGenerator::generateHTML($field, $this->templateType);

            if (!empty($fieldTemplate)) {
                $fieldTemplate = fill_template_with_field_data(
                    $this->commandData->dynamicVars,
                    $this->commandData->fieldNamesMapping,
                    $fieldTemplate,
                    $field
                );
                $fieldTemplate = $this->generateFieldRules($field, $fieldTemplate);
                $this->htmlFields[] = $fieldTemplate;
            }
        }

        $templateData = get_template('scaffold.views.fields', $this->templateType);
        $templateData = fill_template($this->commandData->dynamicVars, $templateData);

        $templateData = str_replace('$FIELDS$', implode("\n\n", $this->htmlFields), $templateData);

        FileUtil::createFile($this->path, 'fields.blade.php', $templateData);
        $this->commandData->commandInfo('field.blade.php created');
    }

    private function generateCreate()
    {
        if($this->isMultiImage){
            $templateData = get_template('scaffold.views.create_multi', $this->templateType);
        }
        else{
            $templateData = get_template('scaffold.views.create', $this->templateType);
        }

        $templateData = fill_template($this->commandData->dynamicVars, $templateData);
        
        if($this->isMultiImage){
            $templateData = str_replace('$FORM_UPLOAD$', ', \'enctype\' => \'multipart/form-data\'', $templateData);
        }
        if($this->isImage || $this->isFile){
            $templateData = str_replace('$FORM_UPLOAD$', ', \'enctype\' => \'multipart/form-data\'', $templateData);
            $templateData = str_replace('$CSS_UPLOAD$', '<link href="{{ asset(\'assets/plugins/fileuploads/css/dropify.min.css\') }}" rel="stylesheet" type="text/css" />', $templateData);
            $templateData = str_replace('$JS_UPLOAD$', '<script src="{{ asset(\'assets/plugins/fileuploads/js/dropify.min.js\') }}"></script>', $templateData);
            $templateData = str_replace('$JS_INIT_UPLOAD$', '$(\'.dropify\').dropify({
                                    messages: {
                                        \'default\': \'Drag and drop a file here or click\',
                                        \'replace\': \'Drag and drop or click to replace\',
                                        \'remove\': \'Remove\',
                                        \'error\': \'Ooops, something wrong appended.\'
                                    },
                                    error: {
                                        \'fileSize\': \'The file size is too big (1M max).\'
                                    }
                                });', $templateData);
        }
        else{
            $templateData = str_replace('$FORM_UPLOAD$', '', $templateData);
            $templateData = str_replace('$CSS_UPLOAD$', '', $templateData);
            $templateData = str_replace('$JS_UPLOAD$', '', $templateData);
            $templateData = str_replace('$JS_INIT_UPLOAD$', '', $templateData);
        }

        if($this->isTextarea)
        {
            $templateData = str_replace('$JS_TEXTAREA$', '<script type="text/javascript" src="{{ asset(\'assets/tinymce/tinymce.js\') }}"></script>', $templateData);
            $templateData = str_replace('$JS_INIT_TEXTAREA$', '$(document).ready(function() {
            tinymce.init({
                selector: \'.tinymce-editor\',
                height: 500,
                theme: \'modern\',
                plugins: [
                    \'advlist autolink lists link image charmap print preview hr anchor pagebreak\',
                    \'searchreplace wordcount visualblocks visualchars code fullscreen\',
                    \'insertdatetime media nonbreaking save table contextmenu directionality\',
                    \'emoticons template paste textcolor colorpicker textpattern imagetools\'
                ],
                toolbar1: \'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image\',
                toolbar2: \'print preview media | forecolor backcolor emoticons\',
                image_advtab: true,
                relative_urls: false
            });
            $(\'#form-parsley\').parsley().on(\'form:validate\', function() {
                $.each($(".tinymce-editor"), function(index, value){
                    var id = $(value).attr("id");
                    $("#"+id).html(tinyMCE.get(id).getContent());
                });
            });
        });', $templateData);
        }
        else{
            $templateData = str_replace('$JS_TEXTAREA$', '', $templateData);
            $templateData = str_replace('$JS_INIT_TEXTAREA$', '', $templateData);
        }

        FileUtil::createFile($this->path, 'create.blade.php', $templateData);
        $this->commandData->commandInfo('create.blade.php created');
    }

    private function generateUpdate()
    {
        if($this->isMultiImage){
            $templateData = get_template('scaffold.views.edit_multi', $this->templateType);
        }
        else{
            $templateData = get_template('scaffold.views.edit', $this->templateType);   
        }

        $templateData = fill_template($this->commandData->dynamicVars, $templateData);

        if($this->isImage || $this->isFile || $this->isMultiImage){
            $templateData = str_replace('$FORM_UPLOAD$', ', \'enctype\' => \'multipart/form-data\'', $templateData);
            $templateData = str_replace('$CSS_UPLOAD$', '<link href="{{ asset(\'assets/plugins/fileuploads/css/dropify.min.css\') }}" rel="stylesheet" type="text/css" />', $templateData);
            $templateData = str_replace('$JS_UPLOAD$', '<script src="{{ asset(\'assets/plugins/fileuploads/js/dropify.min.js\') }}"></script>', $templateData);
            $templateData = str_replace('$JS_INIT_UPLOAD$', '$(\'.dropify\').dropify({
                                    messages: {
                                        \'default\': \'Drag and drop a file here or click\',
                                        \'replace\': \'Drag and drop or click to replace\',
                                        \'remove\': \'Remove\',
                                        \'error\': \'Ooops, something wrong appended.\'
                                    },
                                    error: {
                                        \'fileSize\': \'The file size is too big (1M max).\'
                                    }
                                });', $templateData);
        }
        else{
            $templateData = str_replace('$FORM_UPLOAD$', '', $templateData);
            $templateData = str_replace('$CSS_UPLOAD$', '', $templateData);
            $templateData = str_replace('$JS_UPLOAD$', '', $templateData);
            $templateData = str_replace('$JS_INIT_UPLOAD$', '', $templateData);
        }

        if($this->isTextarea)
        {
            $templateData = str_replace('$JS_TEXTAREA$', '<script type="text/javascript" src="{{ asset(\'assets/tinymce/tinymce.js\') }}"></script>', $templateData);
            $templateData = str_replace('$JS_INIT_TEXTAREA$', '$(document).ready(function() {
            tinymce.init({
                selector: \'.tinymce-editor\',
                height: 500,
                theme: \'modern\',
                plugins: [
                    \'advlist autolink lists link image charmap print preview hr anchor pagebreak\',
                    \'searchreplace wordcount visualblocks visualchars code fullscreen\',
                    \'insertdatetime media nonbreaking save table contextmenu directionality\',
                    \'emoticons template paste textcolor colorpicker textpattern imagetools\'
                ],
                toolbar1: \'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image\',
                toolbar2: \'print preview media | forecolor backcolor emoticons\',
                image_advtab: true,
                relative_urls: false
            });
            $(\'#form-parsley\').parsley().on(\'form:validate\', function() {
                $.each($(".tinymce-editor"), function(index, value){
                    var id = $(value).attr("id");
                    $("#"+id).html(tinyMCE.get(id).getContent());
                });
            });
        });', $templateData);
        }
        else{
            $templateData = str_replace('$JS_TEXTAREA$', '', $templateData);
            $templateData = str_replace('$JS_INIT_TEXTAREA$', '', $templateData);
        }

        if($this->fieldMulti != ""){
            $templateData = str_replace('$FILES_MULTI$', $this->fieldMulti, $templateData);
        }
        else{
            $templateData = str_replace('$FILES_MULTI$', '', $templateData);
        }

        FileUtil::createFile($this->path, 'edit.blade.php', $templateData);
        $this->commandData->commandInfo('edit.blade.php created');
    }

    private function generateShowFields()
    {
        $fieldTemplate = get_template('scaffold.views.show_field', $this->templateType);

        $fieldsStr = '';

        foreach ($this->commandData->fields as $field) {
            $singleFieldStr = str_replace('$FIELD_NAME_TITLE$', Str::title(str_replace('_', ' ', $field->name)),
                $fieldTemplate);
            $singleFieldStr = str_replace('$FIELD_NAME$', $field->name, $singleFieldStr);
            $singleFieldStr = fill_template($this->commandData->dynamicVars, $singleFieldStr);

            $fieldsStr .= $singleFieldStr."\n\n";
        }

        FileUtil::createFile($this->path, 'show_fields.blade.php', $fieldsStr);
        $this->commandData->commandInfo('show_fields.blade.php created');
    }

    private function generateShow()
    {
        $templateData = get_template('scaffold.views.show', $this->templateType);

        $templateData = fill_template($this->commandData->dynamicVars, $templateData);

        FileUtil::createFile($this->path, 'show.blade.php', $templateData);
        $this->commandData->commandInfo('show.blade.php created');
    }

    public function rollback()
    {
        $files = [
            'table.blade.php',
            'index.blade.php',
            'fields.blade.php',
            'create.blade.php',
            'edit.blade.php',
            'show.blade.php',
            'show_fields.blade.php',
        ];

        if ($this->commandData->getAddOn('datatables')) {
            $files[] = 'datatables_actions.blade.php';
        }

        foreach ($files as $file) {
            if ($this->rollbackFile($this->path, $file)) {
                $this->commandData->commandComment($file.' file deleted');
            }
        }
    }

    private function generateFieldRules($field, $fieldTemplate)
    {
        $attributes = "['class' => 'form-control'";
        if($field->htmlType == 'textarea'){
            $attributes = "['class' => 'form-control tinymce-editor'";
        }
        $tempAttrs = explode('|', $field->validations);
        foreach ($tempAttrs as $tempAttr)
        {
            if($tempAttr == 'required')
            {
                $attributes .= ",'data-parsley-required' => 'true'";
            }
            elseif(strpos($tempAttr, 'min') !== false || strpos($tempAttr, 'max') !== false)
            {
                $temp = explode(':', $tempAttr);
                $attributes .= ",'data-parsley-".$temp[0]."length' => '" . $temp[1] . "'";
            }
            elseif(strpos($tempAttr, 'regex') !== false)
            {
                $attributes .= ",'data-parsley-pattern' => '" . str_replace('regex:', '', $tempAttr) . "'";
            }
            elseif(in_array($tempAttr, array('email', 'numeric', 'integer', 'url')))
            {
                if($tempAttr == 'numeric'){
                    $tempAttr = 'number';
                }
                $attributes .= ",'data-parsley-type' => '" . $tempAttr . "'";
            }
            elseif($tempAttr == 'confirmed')
            {
                $attributes .= ",'data-parsley-equalto' => '#" . $field->name . "_confirmation'";
            }
        }
        $attributes .= "]";
        return str_replace('$ATTRIBUTES$', $attributes, $fieldTemplate);
    }
}
