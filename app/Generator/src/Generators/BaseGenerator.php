<?php

namespace InfyOm\Generator\Generators;

use InfyOm\Generator\Utils\FileUtil;
use Log;

class BaseGenerator
{
	/**
     * check image field is exist in fields list
     * @var boolean
     */
    public $isImage = false;

    /**
     * list image fields
     * @var array
     */
    public $imageFields = array();

    /**
     * @var boolean
     */
    public $isTextarea = false;

    /**
     * @var boolean
     */
    public $isMultiImage = false;

    /**
     * @var string
     */
    public $fieldMulti = "";

    /**
     * @var boolean
     */
    public $isFile = false;

    public function __construct($commandData)
    {
    	if(count($commandData->fields) > 0)
        {
            foreach ($commandData->fields as $field) {
                if($field->htmlType == 'image')
                {
                    $this->imageFields[] = $field->name;
                    $this->isImage = true;
                }
                elseif ($field->htmlType == 'file')
                {
                    $this->isFile = true;
                }
                elseif ($field->htmlType == 'textarea') 
                {
                    $this->isTextarea = true;
                }
                elseif ($field->htmlType == 'mimage') 
                {
                    $this->isMultiImage = true;
                    $this->fieldMulti = $field->name;
                }
            }
        }
    }

    public function rollbackFile($path, $fileName)
    {
        if (file_exists($path.$fileName)) {
            return FileUtil::deleteFile($path, $fileName);
        }

        return false;
    }
}
