<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * Class Post
 * @package App\Models\Admin
 * @version June 13, 2017, 9:48 am UTC
 */
class Post extends Model
{

    public $table = 'posts';
    


    public $fillable = [
        'title',
        'content',
        'active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'content' => 'string',
        'active' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required'
    ];

    
}
