<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * Class Common
 * @package App\Models\Admin
 * @version June 13, 2017, 8:01 am UTC
 */
class Common extends Model
{

    public $table = 'commons';
    


    public $fillable = [
        'name',
        'option_value',
        'active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'option_value' => 'string',
        'active' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'option_value' => 'required'
    ];

    
}
