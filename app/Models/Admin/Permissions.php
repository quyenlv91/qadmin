<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * Class Permissions
 * @package App\Models
 * @version September 20, 2016, 4:04 pm UTC
 */
class Permissions extends Model
{

    public $table = 'permissions';
    


    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|unique:permissions,name'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function roles()
    {
        return $this->belongsToMany(\App\Models\Admin\Roles::class, 'role_has_permissions', 'permission_id', 'role_id');
    }
}
