<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * Class Slides
 * @package App\Models\Admin
 * @version October 10, 2016, 9:48 pm UTC
 */
class Slides extends Model
{

    public $table = 'slides';
    


    public $fillable = [
        'title',
        'sub_title',
        'image',
        'url',
        'order',
        'active',
        'old_image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'sub_title' => 'string',
        'image' => 'string',
        'url' => 'string',
        'order' => 'integer',
        'active' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'image' => 'required',
        'url' => 'required',
        'order' => 'integer',
        'active' => 'integer'
    ];

    
}
