<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * Class Roles
 * @package App\Models
 * @version September 20, 2016, 4:01 pm UTC
 */
class Roles extends Model
{

    public $table = 'roles';
    


    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|unique:roles,name'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function permissions()
    {
        return $this->belongsToMany(\App\Models\Admin\Permissions::class, 'role_has_permissions', 'role_id', 'permission_id');
    }
}
