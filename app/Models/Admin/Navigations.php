<?php

namespace App\Models\Admin;

use Eloquent as Model;
use InfyOm\Generator\Generators\Scaffold\MenuGenerator;
use DB;

/**
 * Class Navigations
 * @package App\Models
 * @version September 22, 2016, 8:21 am UTC
 */
class Navigations extends Model
{

    public $table = 'navigations';
    


    public $fillable = [
        'title',
        'url',
        'icon',
        'order',
        'active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'url' => 'string',
        'icon' => 'string',
        'order' => 'integer',
        'active' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'url' => 'required',
        'order' => 'required'
    ];

    public function save(array $option = [])
    {
        $return = parent::save($option);
        $this->updateNavigationLayout();
        return $return;
    }

    public function delete()
    {
        $return = parent::delete();
        $this->updateNavigationLayout();
        return $return;
    }
    
    public static function updateNavigationLayout()
    {
        $path = config('infyom.laravel_generator.path.views', base_path('resources/views/')).config('infyom.laravel_generator.add_on.menu.menu_file');
        $templateType = config('infyom.laravel_generator.templates', 'core-templates');
        // $menuContents = file_get_contents($path);
        $menuTemplate = get_template('scaffold.layouts.menu_template-vsii', $templateType);
        $navigations = DB::table('navigations as n')->select('n.*', DB::raw('GROUP_CONCAT(n1.id) as subs'))->leftJoin('navigations as n1', 'n1.parent_id', '=', 'n.id')->where([['n.active', '=', 1], ['n.parent_id', '=', 0]])->orderBy('n.order', 'asc')->groupBy('n.id')->get()->toArray();
        $menuContents = "";
        if(count($navigations) > 0)
        {
            foreach ($navigations as $navigation) {
                $replaceData = array();
                $replaceData['$ACTIVE_ATTRIBUTE$'] = starts_with($navigation->url, 'http') ? '" target="_blank' : '{{ Request::is(\'*'.$navigation->url.'*\') ? \'active\' : \'\' }}';
                $replaceData['$TITLE$'] = $navigation->title;
                $replaceData['$URL$'] = starts_with($navigation->url, 'http') ? $navigation->url : '{{ url(\'' .$navigation->url . '\') }}';
                $replaceData['$ICON$'] = $navigation->icon;
                $menuContents .= fill_template($replaceData, $menuTemplate);

            }
        }
        else{
            $menuContents = "";
        }
        file_put_contents($path, $menuContents);
    }
}
