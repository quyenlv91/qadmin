<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Spatie\Permission\Traits\HasRoles;
use App\Models\Admin\Permissions;

/**
 * Class Users
 * @package App\Models
 * @version September 19, 2016, 6:24 am UTC
 */
class Users extends Model
{
    use HasRoles;

    public $table = 'users';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'avatar',
        'email',
        'password',
        'active',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'avatar' => 'string',
        'email' => 'string',
        'password' => 'string',
        'password_confirmation' => 'string',
        'active' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required|unique:users',
        'password' => 'required|confirmed|min:6',
        'password_confirmation' => 'required|min:6'
    ];

    public function save(array $options = [])
    {
        if(is_null($this->id)){
            $this->password = bcrypt($this->password);
        }
        return parent::save($options);
    }

    public function roles()
    {
        return $this->belongsToMany(\App\Models\Admin\Roles::class, 'user_has_roles', 'user_id', 'role_id');
    }
}
