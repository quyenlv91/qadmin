<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Common;
use InfyOm\Generator\Common\BaseRepository;

class CommonRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'option_value',
        'active'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Common::class;
    }

    /**
     * apply Search function for index page.
     * @return void
     */
    public function applySearch()
    {
        $s = app('request')->input('s');
        if($s != NULL && strlen(trim($s)) > 0 && count($this->fieldSearchable) > 0)
        {
            if(count($this->fieldSearchable) == 1)
            {
                $this->applyConditions([[$this->fieldSearchable[0], 'like', "%".$s."%"]]);
            }
            else
            {
                $this->model = $this->model->where(function($query) use($s){
                    $i = 0;
                    foreach ($this->fieldSearchable as $field) {
                        if($i == 0)
                        {
                            $query->where($field, 'like', '%'.$s.'%');
                        }
                        else{
                            $query->orWhere($field, 'like', '%'.$s.'%');
                        }
                        $i++;
                    }
                });
            }
        }
    }
}
