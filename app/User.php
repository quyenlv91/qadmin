<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use App\Models\Permissions;

class User extends Authenticatable
{
    use HasRoles;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','avatar',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(\App\Models\Admin\Roles::class, 'user_has_roles', 'user_id', 'role_id');
    }

    public function hasPermission($permission)
    {
        $permissions = [];
        if($this->roles->count() > 0)
        {
            foreach ($this->roles as $role) {
                $permissions = array_merge($permissions, $role->permissions->toArray());
            }
        }
        return collect($permissions)->contains('name', $permission);
    }
}
