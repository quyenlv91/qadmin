/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */

$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#form-parsley').fileupload({
        // Uncomment the following to send cross-domain cookies:
        // xhrFields: {withCredentials: true},
        url: '/admin/upload/images'
    }).bind('fileuploaddone', function (e, data) {
        // console.log(data.getFilesFromResponse(data));
    });

    // Load existing files:
    $('#form-parsley').addClass('fileupload-processing');
    $.ajax({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: 'http://qadmin.local/admin/tests/1/images',
        dataType: 'json',
        context: $('#fileupload')[0]
    }).always(function () {
        $('#form-parsley').removeClass('fileupload-processing');
    }).done(function (result) {
        $('#form-parsley').fileupload('option', 'done')
            .call('#form-parsley', $.Event('done'), {result: result});
    });

});
