/**
 * Theme: Adminto Admin Template
 * Author: Coderthemes
 * SweetAlert
 */

!function ($) {
    "use strict";

    var SweetAlert = function () {
    };

    //examples 
    SweetAlert.prototype.init = function () {

        //Warning Message
        $('.sa-warning').click(function () {
            var thisForm = $(this).parents('form');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this record file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function (inputValue) {
                if(inputValue === true)
                {
                    thisForm.submit();
                }
            });
        });

        $('#btn-group-action').click(function () {
            var checkedInput = $(".chk-ele:checked");
            if(checkedInput.length == 0){
                swal("Please choose least a checkbox!");
            }
            else{
                var thisForm = $(this).parents('form');
                $.each(checkedInput, function(i, v){
                    thisForm.append($(v).clone().hide());
                });
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this records!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-warning',
                    confirmButtonText: "Yes!",
                    closeOnConfirm: false
                }, function (inputValue) {
                    if(inputValue === true)
                    {

                        thisForm.submit();
                    }
                });
            }
        });
    },
        //init
        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
}(window.jQuery),

//initializing 
    function ($) {
        "use strict";
        $.SweetAlert.init()
    }(window.jQuery);